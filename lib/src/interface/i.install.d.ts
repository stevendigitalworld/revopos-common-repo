export interface Install {
    system?: string;
    channel?: string;
    target_version?: string;
    is_latest?: boolean;
    changelog?: string;
    error?: string;
}
