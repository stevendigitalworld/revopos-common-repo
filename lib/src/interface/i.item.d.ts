import { IPackage } from './i.package';
import { Combo } from './../class/combo-model';
import { ModifierModel } from "../class/modifier-model";
import { UserModel } from "../class/user-model";
export interface IBaseItem {
    id?: number | undefined;
    image_path?: string | undefined;
    codename?: string | undefined;
    name?: string | undefined;
    alias?: string | undefined;
    description?: string | undefined;
    type?: number | undefined;
    printer_id?: string | undefined;
    category_id?: number | undefined;
    unit?: number | undefined;
    stock?: number | undefined;
    tax_rate?: number | undefined;
    tax_method?: number | undefined;
    is_print?: number | undefined;
    price?: number | undefined;
    price_point?: number | undefined;
    discountable?: boolean | undefined;
    modifier?: Array<ModifierModel>;
    user?: Array<UserModel>;
    is_modifier_group?: boolean;
    credit?: number | undefined;
    is_tax_exempt?: boolean | undefined;
    is_service_charge_exempt?: boolean | undefined;
    discount_calc_type?: number | undefined;
    discount_amount?: number | undefined;
    discount_percent?: number | undefined;
    attribute?: any;
    variant?: any;
    package?: IPackage;
}
export interface IItem extends IBaseItem {
    combo: Combo;
}
