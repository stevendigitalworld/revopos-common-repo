export interface IRegister {
    name?: string;
    phone_country_code?: string;
    phone_no?: string;
    password?: string;
    otp?: string;
    email?: string;
}
