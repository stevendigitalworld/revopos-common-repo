import { IVersionRegistry } from "./i.version.registry";
export interface IVersion {
    version_registry?: Array<IVersionRegistry>;
}
