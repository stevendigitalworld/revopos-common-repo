/** Extra information about invoice */
export interface IEtc {
    /** Reason for voiding the invoice */
    void_reason?: string | undefined;
    /** Date and time the invoice is being voided */
    voided_at?: Date | undefined;
    /** The user id who voided the invoice */
    voided_by?: Number | undefined;
    /** If true, another invoice is cloned from this invoice */
    is_parent?: boolean | undefined;
    /** if is_parent is true,this invoice is a parent invoice from which a child invoice is cloned,
then there will be a child invoice id */
    child_invoice_id?: number | undefined;
    /** If true, another invoice is cloned from this invoice */
    is_child?: boolean | undefined;
    /** if is_child is true, this invoice is child invoice cloned from a parent invoice
then there will be a parent invoice id */
    parent_invoice_id?: number | undefined;
}
