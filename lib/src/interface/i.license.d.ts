export interface ILicense {
    api_key?: string | undefined;
    code?: number | undefined;
    company_name?: string | undefined;
    datetime_activation?: Date;
    datetime_expiry?: Date;
    datetime_issue?: Date;
    db_name?: string | undefined;
    db_pwd?: string | undefined;
    db_user?: string | undefined;
    error?: any | undefined;
    fingerprint?: string | undefined;
    id?: number | undefined;
    latest_activated_window?: number | undefined;
    license?: string | undefined;
    plan?: number | undefined;
    quotas?: any;
    signature?: string | undefined;
}
