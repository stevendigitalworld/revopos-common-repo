import { ICoupon } from './i.coupon';
export interface ICouponPurchaseItemized {
    coupon: ICoupon | undefined;
    coupon_spend_itemized: any[];
    coupon_id: number;
    customer_id: number;
    user_id: number;
    promo_code_type: number;
    promo_code: string;
    datetime_start: string;
    datetime_end: string;
    quantity: number;
    usage_count: number;
    datetime_create: string;
    timestamp_update: number;
    is_expired: boolean;
    is_spend: boolean;
    is_generated: boolean;
    is_delete: boolean;
    guid: string;
}
