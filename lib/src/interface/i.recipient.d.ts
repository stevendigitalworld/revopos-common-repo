export interface IRecipient {
    name?: string;
    to_address?: string;
}
