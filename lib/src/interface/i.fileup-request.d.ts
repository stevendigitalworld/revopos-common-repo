export interface IFileUploadRequst {
    name?: string;
    type?: number;
    base64?: string;
    format?: string;
}
