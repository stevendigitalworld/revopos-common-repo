export interface IAuth {
    phone_no?: string | undefined;
    phone_country_code?: string | undefined;
    request?: number | undefined;
}
