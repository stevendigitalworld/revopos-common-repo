export declare enum CouponAction {
    Validate = 1,
    Generate = 2,
    Purchase = 3
}
