export declare enum TillActionCode {
    open_drawer = 1,
    pay_in = 2,
    pay_out = 3
}
