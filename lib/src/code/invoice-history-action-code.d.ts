export declare enum InvoiceHistoryActionCode {
    ReprintKitchenSlip = 1,
    ReprintOrderSlip = 2,
    ReprintAll = 3
}
