import { IDelivery } from './../interface/i.delivery';
export declare class DeliveryModel implements IDelivery {
    action?: number;
    datetime_order?: any;
    delivery_by?: any;
    id?: number;
    invoice?: any;
    invoice_id?: number;
    is_delete?: boolean;
    json?: string;
    server_delivery_id?: number;
    status?: number;
}
