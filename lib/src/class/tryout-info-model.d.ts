import { ITryOutInfo } from './../interface/i.tryout-info';
export declare class TryOutInfoModel implements ITryOutInfo {
    email: any;
    last_name: any;
    first_name: any;
    company: any;
    address: any;
    zip?: number;
    country: any;
    city: any;
    constructor();
}
