export declare class InvoiceDiscountModel {
    id?: number;
    invoice_id?: number;
    name?: string;
    discount_calc_type?: any;
    discount_percent?: number;
    discount_amount?: number;
    datetime_create?: any;
    coupon_purchase_itemized_id?: number;
    constructor(invoice: any, coupon_purchase: any, discount_amount: any);
}
