import { IVersion } from "../interface/i.version";
import { IVersionRegistry } from "../interface/i.version.registry";
export declare class VersionModel implements IVersion {
    version_registry?: IVersionRegistry[];
}
