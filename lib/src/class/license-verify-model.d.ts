import { ILicenseVerify } from './../interface/i.license.verify';
export declare class LicenseVerifyModel implements ILicenseVerify {
    action?: number;
    quotas: any;
    totp?: string;
}
