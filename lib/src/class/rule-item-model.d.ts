import { ItemModel } from "./item-model";
export declare class RuleItemModel {
    category_id?: number;
    datetime_create?: string;
    id?: number;
    is_delete?: boolean;
    item?: ItemModel[];
    item_id?: number;
    quota_order?: number;
    rule_id?: number;
    constructor();
}
