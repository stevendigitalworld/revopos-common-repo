import { IAttribute, IAttributeItem, IVariantDefinition, IVariantItem } from '../interface/i.attribute';
export declare class Attribute implements IAttribute {
    attribute_item: Array<IAttributeItem>;
    error: any;
    id: Number;
    name: String;
    alias: String;
}
export declare class AttributeItem implements IAttributeItem {
    id: Number;
    attribute_id: Number;
    name: String;
    alias: String;
}
export declare class Variant {
    item: IVariantItem;
    variant_definition: Array<IVariantDefinition>;
}
