import { IComboGroup } from './../interface/i.combo-group';
import { ComboItemModel } from './combo-item-model';
export declare class ComboGroupModel implements IComboGroup {
    combo_item: Array<ComboItemModel>;
    id?: number;
    combo_id?: number;
    name?: string;
    quota?: number;
    is_delete?: false;
    sequence: number;
}
