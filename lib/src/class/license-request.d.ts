import { ILicenseRequest } from "../interface/i.license.request";
export declare class LicenseRequest implements ILicenseRequest {
    action?: number;
    license?: string;
    cpu_id?: string;
    ip?: string;
    constructor();
}
