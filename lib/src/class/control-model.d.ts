import { IControl } from './../interface/i.control';
import { ControlActionCode } from './../code/control-action-code';
export declare class ControlModel implements IControl {
    version?: string;
    platform?: string;
    platform_version?: number;
    uid?: string;
    type?: number;
    action?: ControlActionCode;
    constructor(device_type: number, config_version: string);
}
