export declare class InvoiceLineDiscountModel {
    id?: number;
    invoice_line_id: number;
    name: string;
    discount_calc_type: any;
    discount_percent: number;
    discount_amount: number;
    datetime_create: any;
    coupon_purchase_itemized_id: number;
    constructor(line: any, coupon_purchase: any, discount_amount: any);
}
