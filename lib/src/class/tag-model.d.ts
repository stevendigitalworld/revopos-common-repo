import { ITag } from "../interface/i.tag";
export declare class TagModel implements ITag {
    id?: number;
    name?: string;
    model?: string;
}
