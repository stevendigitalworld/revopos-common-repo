import { InvoiceItemizedModel } from './invoice-itemized-model';
import { IInvoiceLine } from '../interface/i.invoice.line';
export declare class InvoiceLineModel implements IInvoiceLine {
    id: number;
    item?: any;
    invoice_itemized?: InvoiceItemizedModel[];
    invoice_id?: number;
    item_id?: number;
    uom_type?: number;
    subtotal?: number;
    quantity?: number;
    weight?: number;
    state: number;
    is_delete: boolean;
    is_take_out: boolean;
    remark: any;
    discount_percent: number;
    discount_amount: number;
    discount_calc_type: number;
    subtotal_before_discount: number;
    combo_actual_price: number;
    constructor(item: any, quantity: number, itemized?: any);
}
