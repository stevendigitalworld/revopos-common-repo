import { IGetOTP } from "../interface/i.get.otp";
export declare class GetOTPModel implements IGetOTP {
    customer: {
        phone_no: string;
        phone_country_code: string;
    };
    shop_license_id: number;
    request_code: number;
}
