import { IReportRequest } from './../interface/i.report-request';
import { EmailModel } from './email-model';
export declare class ReportRequest implements IReportRequest {
    type?: number;
    datetime_start?: string;
    datetime_end?: string;
    operation: {};
    template: any;
    action?: number;
    email?: EmailModel;
    automate?: boolean;
    schedule_type?: number;
    constructor();
}
