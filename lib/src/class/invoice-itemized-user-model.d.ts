export declare class InvoiceItemizedUserModel {
    itemized?: any;
    user?: any;
    id: number;
    invoice_itemized_id: number;
    user_id: number;
    constructor(itemized?: any, user?: any);
}
