import { ISection } from "../interface/i.section";
export declare class SectionModel implements ISection {
    id?: number;
    is_delete?: boolean;
    name?: string;
    constructor();
}
