import { ILicense } from "../interface/i.license";
export declare class LicenseModel implements ILicense {
    api_key?: string;
    code?: number;
    company_name?: string;
    datetime_activation?: Date;
    datetime_expiry?: Date;
    datetime_issue?: Date;
    db_name?: string;
    db_pwd?: string;
    db_user?: string;
    error?: any;
    fingerprint?: string;
    id?: number;
    latest_activated_window?: number;
    license?: string;
    plan?: number;
    quotas?: any;
    signature?: string;
}
