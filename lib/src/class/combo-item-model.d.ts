import { IComboItem } from './../interface/i.combo-item';
import { IBaseItem } from "../interface/i.item";
export declare class ComboItemModel implements IComboItem {
    item?: IBaseItem;
    id?: number;
    item_id?: number;
    combo_group_id?: number;
    price?: number;
    is_delete?: false;
}
