import { ICustomer } from "../interface/i.customer";
export declare class CustomerModel implements ICustomer {
    id?: number;
    display_name?: string;
    ic?: string;
    phone_no?: string;
    language?: string;
    phone_country_code?: string;
    phone_number?: string;
    ref_id?: string;
    address?: string;
    email?: string;
    birth_date?: any;
    constructor();
}
