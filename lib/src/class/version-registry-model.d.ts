import { IVersionRegistry } from "../interface/i.version.registry";
export declare class VersionRegistryModel implements IVersionRegistry {
    app_version?: string;
    datetime_create?: Date;
    download_url?: string;
    id?: number;
    label?: string;
    package_hash?: string;
    package_size?: number;
    plugin_version?: string;
    server_version?: any;
    system?: number;
    version?: any;
}
