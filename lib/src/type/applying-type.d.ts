export declare enum ApplyingType {
    All = 1,
    Inclusive = 2,
    Exclusive = 3
}
