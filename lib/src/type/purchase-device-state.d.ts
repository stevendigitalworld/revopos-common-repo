export declare enum PurchaseDeviceState {
    AutoDetectError = 1,
    ConnectToServer = 2,
    DisconnectToServer = 3,
    Activated = 4
}
