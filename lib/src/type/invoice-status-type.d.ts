export declare enum InvoiceStatusType {
    Open = 0,
    Onhold = 1,
    Payment = 2,
    Close = 3,
    Void = 4
}
