export declare enum DeviceType {
    Unknown = 0,
    Revopos = 1,
    Emenu = 2,
    HotPotEmenu = 3,
    CustomerDisplay = 4,
    Queue = 5,
    KitchenDisplay = 6
}
