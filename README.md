

## 如何编写代码，并打包发布
1. 在src的文件夹中，更新或增加数据模型（新增的模型不要忘记在index.ts导出）  
2. 如需要发布，则更新package.json 中的版本号  
3. 跑命令 `npm run build`   
4. 手写更改的代码和打包完成后的lib中的改动，通通提交并推送  
5. 标签： `git tag 3.xx.xx`  `git push --tags`  标签与package.json中version保持一致 

## 如何引用 
1. 在需要使用的项目中安装   
`npm i git+https://bitbucket.org/stevendigitalworld/revopos-common-repo.git#3.xx.xx`    

2. 在需要的使用的ts文件中引入   
```js
import { IConfig,InvoiceModel } from 'revopos-common-repo/lib';
``` 

3. 即可使用  
例如
```js
let config = await this.config_svp._ReadAsync() as IConfig;
let model = new InvoiceModel();
```

