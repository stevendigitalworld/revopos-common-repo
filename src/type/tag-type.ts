export enum TagType {
    report = 'report',
    template = 'template',
    potslider = 'potslider',
    cdsfull = 'cdsfull',
    cdshalf = 'cdshalf'
}