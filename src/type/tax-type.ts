export enum TaxType {
    Inclusive = 1,
    Exclusive = 2
}