export enum CalcType {
    FixedAmount = 1,
    Percentage = 2
}
