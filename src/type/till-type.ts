export enum TillType {
    initial_deposit = 1,
    invoice = 2,
    pay_in = 3,
    pay_out = 4,
}