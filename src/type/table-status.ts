export enum TableStatus {
    Closed = 1,
    Opened = 2,
    Reserved = 3,
    Payment = 4,
    Onhold = 5,
    MultipleStatus = 6,
    Locked = 7,
    OpenedWithAnotherReservationOnHold = 8
}
