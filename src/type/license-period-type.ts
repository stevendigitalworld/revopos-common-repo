export enum LicensePeriodType {
    Month = 1,
    Year = 2
}
