export enum KitchenItemStatus {
    NEW_ORDER = 1,
    IN_PREPARE = 2,
    PREPARED = 3,
    COMPLETED = 4
}