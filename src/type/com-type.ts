export enum ComType {
    internal = 1,
    signalr = 2,
    invoices = 3,
    invoice = 4,
    current_user = 5,
    refresh = 6,
    lock = 7,
    unlock = 8,
    unlock_screen = 9,
    lock_screen = 10
}
