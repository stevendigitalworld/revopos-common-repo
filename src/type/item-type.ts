export enum ItemType {
    ItemByQuantity = 1,
    ItemByWeight = 2,
    Service = 3,
    Package = 4,
    Combo = 5,
    Attribute = 6,
    Variant = 7
}
