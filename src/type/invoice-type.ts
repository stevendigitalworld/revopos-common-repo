export enum InvoiceType {
    DineIn = 1,
    TakeAway = 2,
    Delivery = 3,
}
