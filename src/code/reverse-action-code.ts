export enum ReverseActionCode {
    CreateReservation = 1,
    StartReservation = 2,
    CancelReservation = 3,
    OnHoldReservation = 4,
    RevertOnHoldReservation = 5
}