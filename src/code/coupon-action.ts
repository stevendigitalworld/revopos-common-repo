export enum CouponAction {
    Validate = 1,  // 验证
    Generate = 2, // 生成, APP 接口没有此功能
    Purchase = 3,  // 购买
}
