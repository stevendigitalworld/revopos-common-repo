export enum ReportActionCode {
    Print = 0,
    Generate = 1,
    Email = 2
}