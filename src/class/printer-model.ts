import { IPrinter } from './../interface/i.printer';
export class PrinterModel implements IPrinter{
    type?: number;
    name?: string;
    printer_name?: string;
    is_zpl?: boolean;
    template_filename?: any;
    backup_printer_id?: number;
    is_delete?: boolean;
    terminal_id?: number;
    id?: number;
    constructor() { }
}


