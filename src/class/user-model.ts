import { IUser } from "../interface/i.user";

export class UserModel implements IUser {
    id?: number;
    user_id: string = "";
    name?: string;
    email?: string;
    password?: string;
    role?: number;
    is_delete: number = 0;
    edit?: boolean;
}