import { ICategory } from './../interface/i.category';
export class CategoryModel implements ICategory{
    name?: string;
    description?: string;
}
