import { IReportRequest } from './../interface/i.report-request';
import { EmailModel } from './email-model';

export class ReportRequest implements IReportRequest{
    public type?: number;
    public datetime_start?: string;
    public datetime_end?: string;
    public operation = {};
    public template: any;
    public action?: number;
    public email?: EmailModel;
    public automate?: boolean;
    public schedule_type?: number;
    constructor() { }
}
