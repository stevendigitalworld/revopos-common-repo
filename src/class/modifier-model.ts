import { IModifier } from "../interface/i.modifier";

export class ModifierModel implements IModifier {
    group: number = 0;
    group_quota: number = 0;
    price: number = 0;
    name?: string;
    is_print: boolean = true;
    item_id?: number;
    is_delete?: boolean = false;
    is_internal?: boolean = false;
    item_modifier_id?: number;
    user_id?: any;
    constructor(is_delete?: boolean, item_modifier_id?: number, user_id?: number) {
       if(is_delete) this.is_delete = is_delete;
       if(item_modifier_id) this.item_modifier_id = item_modifier_id;
       if(user_id) this.user_id = user_id;
    }
}
