import { IFileUploadRequst } from './../interface/i.fileup-request';

export class FileUploadRequst implements IFileUploadRequst{
    name?: string;
    type?: number;
    base64?: string;
    format?: string;
    constructor() { }
}
