import { IPackageItem } from './../interface/i.package.item';
import { IPackage } from '../interface/i.package';

export class Package implements IPackage {
    package_item: IPackageItem[] = [];
    item_id?: number;
    price?: number;
    is_delete?: boolean;
}
