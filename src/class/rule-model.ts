import { IRule } from "../interface/i.rule";
import { RuleItemModel } from "./rule-item-model";

export class RuleModel implements IRule {
    above_invoice_amount?: number;
    applying_type?: number;
    datetime_create?: Date;
    discount_amount?: number;
    discount_calc_type?: number;
    discount_percent?: number;
    id?: number;
    is_delete?: boolean;
    model?: number;
    rule_item?: RuleItemModel[];
    type?: number;
    constructor() { }
}