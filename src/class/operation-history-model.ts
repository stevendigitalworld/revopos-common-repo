import { IOperationHistory } from "../interface/i.operation.history";

export class OperationHistoryModel implements IOperationHistory
{
    amount?: number;
    action?: number;
    id?: number;
    user_id?: number;
    open_datetime?: Date;
    close_datetime?: Date;
    amount_declare?: number;
    amount_record?: number;
    status?: number;
    is_delete?: boolean;
    
    constructor() {}
}
