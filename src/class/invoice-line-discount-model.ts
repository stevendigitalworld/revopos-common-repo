export class InvoiceLineDiscountModel {
    public id?: number;
    public invoice_line_id: number;
    public name: string;
    public discount_calc_type: any;
    public discount_percent: number;
    public discount_amount: number;
    public datetime_create: any;
    public coupon_purchase_itemized_id: number;
    constructor(line: any, coupon_purchase: any, discount_amount: any) {
        this.invoice_line_id = line.id;
        this.coupon_purchase_itemized_id = coupon_purchase.id;
        this.name = coupon_purchase.coupon.name;
        this.discount_calc_type = coupon_purchase.coupon.discount_calc_type;
        this.discount_percent = coupon_purchase.coupon.discount_percent;
        this.discount_amount = discount_amount;
    }
}
