import { ComType } from '../type/com-type';


export class ComDataModel {
    public com_type?: ComType;
    public payload?: any;
}
