import { IPayment } from "../interface/i.payment";

export class PaymentModel implements IPayment {
    id?: number;
    invoice_id?: number;
    tender_amount?: number;
    tender_amount_alt: number = 0;
    change_amount?: number;
    change_amount_alt: number = 0;
    payment_method_id?: number;
    note?: string;
    custom?: string;
    is_delete: boolean = false;
    payment_method?: string;
    rest?: number;
    is_cash?: boolean;

    constructor() { }
}
