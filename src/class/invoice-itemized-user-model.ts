export class InvoiceItemizedUserModel {
    public id: number = 0;
    public invoice_itemized_id: number = 0;
    public user_id: number = 0;

    constructor(public itemized?: any, public user?: any) {
        if (!itemized && !user) {
            this.id = 0;
            this.invoice_itemized_id = itemized.id;
            this.user_id = user.id;
        }
    }
}
