import { IAuth } from "../interface/i.auth";

export class AuthModel implements IAuth{
    phone_no?: string;
    phone_country_code?: string;
    request?: number;
}