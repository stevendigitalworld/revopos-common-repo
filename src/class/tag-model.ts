import { ITag } from "../interface/i.tag";

export class TagModel implements ITag {
    id?: number;
    name?: string;
    model?: string;
}