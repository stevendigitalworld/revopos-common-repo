import { IUserCapability } from "../interface/i.user.capability";

export class UserCapabilityModel implements IUserCapability {
    public enable_open_drawer?: boolean;
    public display_till_balance?: boolean;
    public enable_modify_layout?: boolean;
    public management_modify?: boolean;
    public setting_modify?: boolean;
    public crm_modify?: boolean;

    constructor(public role?: number) {
        if (role) {
            if (role == 1) {
                this.enable_open_drawer = true;
                this.display_till_balance = true;
                this.enable_modify_layout = true;
                this.management_modify = true;
                this.setting_modify = true;
                this.crm_modify = true;
            }
            else if (role == 2) {
                this.enable_open_drawer = true;
                this.display_till_balance = true;
                this.enable_modify_layout = true;
                this.management_modify = true;
                this.setting_modify = false;
                this.crm_modify = true;
            }
            else if (role == 3) {
                this.enable_open_drawer = false;
                this.display_till_balance = false;
                this.enable_modify_layout = false;
                this.management_modify = false;
                this.setting_modify = false;
                this.crm_modify = false;
            }
            else {
                this.Default();
            }
        }
        else {
            this.Default();
        }
    }

    private Default() {
        this.enable_open_drawer = false;
        this.crm_modify = false;
    }


}
