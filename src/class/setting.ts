import { ISetting } from "../interface/i.setting";

export class SettingModel implements ISetting {
    id?: number;
    setting?: string;
    company_name?: string;
    gst_reg_no?: string;
    postcode?: string;
    address?: string;
    tel?: string;
    country_name?: string;
}
