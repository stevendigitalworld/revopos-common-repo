export class UICtrlModel
{
    alert_translate?: string;
    alert_subtitle?: boolean;
    alert_subtitle_id?: number;
    modal_name?: string;
    modal_class?: string;
}
