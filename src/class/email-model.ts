import { IEmail } from './../interface/i.email';
import { IRecipient } from '../interface/i.recipient';

export class EmailModel implements IEmail{
    recipients?: IRecipient[] = [];
    attachment?: string[];
    subject?: string;
    content?: string;
    constructor() { }
}
