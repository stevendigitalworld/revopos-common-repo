import { INote } from "../interface/i.note";

export class NoteModel implements INote {
    datetime?: Date;
    group_id?: number;
    id?: number;
    is_delete?: boolean;
    note?: string;
    type?: number;
    usage?: number;
    constructor() { }
}