import { InvoiceItemizedUserModel } from './invoice-itemized-user-model';
import { InvoiceItemizedModifierModel } from './invoice-itemized-modifier-model';
import { IInvoiceItemized } from '../interface/i.invoice.itemized';

export class InvoiceItemizedModel implements IInvoiceItemized{
    id?: number;
    invoice_itemized_modifier?: InvoiceItemizedModifierModel[];
    invoice_itemized_user?: InvoiceItemizedUserModel[];
    invoice_line_id?: number;
    actual_price?: number;
    actual_price_before_discount?: number;
    discount_calc_type: number = 2;
    discount_amount: number = 0;
    discount_percent: number = 0;
    unit_price: number;
    remark: any;
    is_delete: number;
    state: number;
    customer_id: number;
    guid: any;
    is_take_out: boolean = false;
    constructor(item: any){
        this.id = 0;
        this.invoice_itemized_modifier = [];
        this.invoice_itemized_user = [];
        this.invoice_line_id = 0;
        this.actual_price = item.price;
        this.unit_price = item.price;
        this.actual_price_before_discount = item.price;
        this.discount_calc_type = 2;
        this.discount_amount = 0;
        this.discount_percent = 0;
        this.remark = null;
        this.is_delete = 0;
        this.state = 0;
        this.customer_id = 0;
    }
}
