import { IPackageItem } from "../interface/i.package.item";

export class PackageItem implements IPackageItem {
    item_id: number = 0;
    package_id: number = 0;
    quantity?: number;
    is_delete?: boolean;
}
