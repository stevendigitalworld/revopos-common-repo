import { Etc } from './etc-model';
import { IInvoicePayment } from './../interface/i.invoice-payment';
import { InvoiceSignatureModel } from './invoice-signature-model';
import { InvoiceLineModel } from './invoice-line-model';
import { IInvoice } from '../interface/i.invoice';
export class InvoiceModel implements IInvoice{
    id?: number = 0;
    device_id?: number;
    user_id?: number;
    invoice_signature?: InvoiceSignatureModel[];
    payment?: IInvoicePayment[];
    table_switch_id?: number;
    ref_id?: any;
    table_id?: number;
    type?: number = 0;
    pax?: number;
    adult_pax?: number;
    child_pax?: number;
    baby_pax?: number;
    subtotal?: number;
    service_charge?: number;
    discount?: number = 0;
    discount_type?: number = 0;
    discount_amount?: number = 0;
    tax?: number;
    tip?: number;
    rounding?: number;
    grand_total?: number;
    customer_id?: number;
    voucher_id?: number;
    status?: number;
    datetime_open?: Date;
    datetime_close?: Date;
    invoice_line?: InvoiceLineModel[];
    action?: number;
    deposit?: number = 0;
    device_uuid?: any;
    kitchen_print_override?: boolean;
    etc?:Etc;
    constructor(table?: any, uuid?: any) {
        if (table) this.table_id = table.id, this.invoice_line = [], this.type = 1;
        if (uuid) this.device_uuid = uuid;
    }
}
