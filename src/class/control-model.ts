import { IControl } from './../interface/i.control';
import { ControlActionCode } from './../code/control-action-code';
export class ControlModel implements IControl{
    public version?: string;
    public platform?: string;
    public platform_version?: number;
    public uid?: string;
    public type?: number;
    public action? : ControlActionCode;
    constructor(device_type : number, config_version: string) {
        this.type = device_type;
        this.version = config_version;
    }
}
