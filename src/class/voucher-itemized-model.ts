import { IVoucherItemized } from "../interface/i.voucher.itemized";

export class VoucherItemizedModel implements IVoucherItemized {
    voucher_id?: number;
    quantity?: number;
    discount_amount?: number;
}
