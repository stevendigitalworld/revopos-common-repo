import { IAttribute, IAttributeItem, IVariantDefinition, IVariantItem } from '../interface/i.attribute';

export class Attribute implements IAttribute {
    attribute_item: Array<IAttributeItem> = [];
    error: any = null;
    id: Number = 0;
    name: String = "";
    alias: String = "";
}

export class AttributeItem implements IAttributeItem {
    id: Number = 0;
    attribute_id: Number = 0;
    name: String = "";
    alias: String = "";
}

export class Variant {
    item: IVariantItem = {
        name: "",
        alias: "",
        price: 0,
        cost: 0,
        stock: 0,
        codename: "",
        barcode: "",
        expand: false
    };
    variant_definition: Array<IVariantDefinition> = [];
}

