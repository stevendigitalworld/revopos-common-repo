import { InvoiceItemizedModel } from './invoice-itemized-model';
import { IInvoiceLine } from '../interface/i.invoice.line';
import { ItemType } from '../type/item-type';

export class InvoiceLineModel implements IInvoiceLine {
    id: number = 0;
    item?: any;
    invoice_itemized?: InvoiceItemizedModel[];
    invoice_id?: number;
    item_id?: number;
    uom_type?: number;
    subtotal?: number;
    quantity?: number;
    weight?: number;
    state: number = 0;
    is_delete: boolean = false;
    is_take_out: boolean = false;
    remark: any = null;
    discount_percent: number = 0;
    discount_amount: number = 0;
    discount_calc_type: number = 2;
    subtotal_before_discount: number = 0;
    combo_actual_price: number = 0;
    constructor(item: any, quantity: number, itemized?: any) {
        this.item = item;
        this.uom_type = 1;
        this.item_id = item.id;
        this.quantity = 0;
        this.subtotal = +(((+quantity) * item.price).toFixed(2));
        this.invoice_itemized = [];
        this.invoice_itemized.push(itemized);
        if (item.discount_calc_type == 2 && item.discount_percent != 0 && item.discountable && item.type != ItemType.ItemByWeight) {
            this.discount_calc_type = 2;
            this.discount_percent = item.discount_percent;
        }
    }
}
