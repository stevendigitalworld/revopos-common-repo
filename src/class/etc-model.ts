import { IEtc } from "../interface/i.etc";
export class Etc implements IEtc {
    /** Reason for voiding the invoice */
    void_reason?: string | undefined;
    /** Date and time the invoice is being voided */
    voided_at?: Date ;
    /** The user id who voided the invoice */
    voided_by?: Number;
    /** If true, another invoice is cloned from this invoice */
    is_parent?: boolean;
    /** if is_parent is true,this invoice is a parent invoice from which a child invoice is cloned,
then there will be a child invoice id */
    child_invoice_id?: number;
    /** If true, another invoice is cloned from this invoice */
    is_child?: boolean;
    /** if is_child is true, this invoice is child invoice cloned from a parent invoice
then there will be a parent invoice id */
    parent_invoice_id?: number;
}