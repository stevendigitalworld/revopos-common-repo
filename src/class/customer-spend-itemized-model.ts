export class CustomerSpendItemizedModel {
    customer_purchase_id: number;
    item_id: number;
    quantity: number;

    constructor(p: any, qty: number) {
        this.customer_purchase_id = p.customer_purchase_id;
        this.item_id = p.item_id;
        this.quantity = qty;
    }

}
