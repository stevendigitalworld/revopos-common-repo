import { IServerVersion } from "../interface/i.server-version";

export class ServerVersionModel implements IServerVersion{
    channel?: string;
    datetime?: string;
    device?: any;
    is_delete?: boolean;
    revopos_database_current?: string;
    revopos_database_target?: string;
    revopos_server?: string;
    revopos_electron_current?:any;
    revopos_electron_target?: any;
}