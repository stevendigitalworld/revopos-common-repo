export class InvoiceItemizedModifierModel {
    public id?: number;
    public invoice_itemized_id?: number;
    public item_modifier_id?: number;
    public is_delete?: boolean;

    constructor(public itemized?: any, public modifier?: any) {
        if (!itemized || !modifier) {
            this.id = 0;
            this.invoice_itemized_id = itemized.id;
            this.item_modifier_id = modifier.id;
            this.is_delete = false;
        }
    }
}
