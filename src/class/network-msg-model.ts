import { INetworkMsg } from './../interface/i.network-msg';
export declare class NetworkMsgModel implements INetworkMsg{
    ip?:string;
    data?: {
        is_online?: boolean;
        is_retrying?: boolean;
    }
    constructor();
}
