import { ILicenseVerify } from './../interface/i.license.verify';
export class LicenseVerifyModel implements ILicenseVerify {
    action?: number;
    quotas: any = [];
    totp?: string;
}
