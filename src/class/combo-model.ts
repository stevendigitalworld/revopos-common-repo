import { ICombo } from './../interface/i.combo';
import { ComboGroupModel } from './combo-group-model';
export class Combo implements ICombo{
    combo_group : Array<ComboGroupModel> =  [];
    id?: number;
    is_delete?: false;
    is_group?: true;
    is_one_click?: false;
    is_upsizeable?: false;
    item_id?: number;
    upsize_price?: number;
    
    constructor(itemId?: number) {
        if(itemId) this.item_id = itemId;
    }
}
