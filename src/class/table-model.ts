import { ReserveModel } from './reserve-model';
import { ITable } from '../interface/i.table';
export class TableModel implements ITable {
    reservation?: ReserveModel[];
    printer_id_order_list?: any[];
    total?: number;
    pax?: number;
    rows?: number;
    cols?: number;
    id?: number;
    invoice_id?: number;
    section_id?: number;
    name?: string;
    status?: number;
    sizex?: number;
    sizey?: number;
    is_take_out?: boolean;
    is_delete?: boolean;
}