import { IReserve } from './../interface/i.reserve';
export class ReserveModel implements IReserve{
    table_id?: number;
    deposit?: number;
    note?: string;
    datetime_appt?: string;
    action?: number;
    id?: number;
    is_refund?: boolean;
    payment_method_id: any;
}