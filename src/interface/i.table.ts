import { ReserveModel } from "../class/reserve-model";

export interface ITable {
    reservation?: Array<ReserveModel>;
    printer_id_order_list?: any[] | undefined;
    total?: number | undefined;
    pax?: number | undefined;
    rows?: number | undefined;
    cols?: number | undefined;
    id?: number | undefined;
    invoice_id?: number | undefined;
    section_id?: number | undefined;
    name?: string | undefined;
    status?: number | undefined;
    sizex?: number | undefined;
    sizey?: number | undefined;
    is_take_out?: boolean | undefined;
    is_delete?: boolean | undefined;
}