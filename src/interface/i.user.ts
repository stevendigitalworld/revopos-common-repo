export interface IUser {
    id?: number | undefined;
    user_id?: string | undefined;
    name?: string | undefined;
    email?: string | undefined;
    password?: string | undefined;
    role?: number | undefined;
    is_delete?: number | undefined;
    edit?: boolean | undefined;
}