export interface ISection {
    id?: number | undefined;
    is_delete?: boolean | undefined;
    name?: string | undefined;
}