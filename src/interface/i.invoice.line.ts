
export interface IInvoiceLine {
    id?: number | undefined;
    item?: any | undefined;
    item_id?: number | undefined;
    quantity?: number | undefined;
    subtotal?: number | undefined;
    invoice_itemized?: any[];
    uom_type?: number | undefined;
    weight?: number | undefined;
    state: number;
    is_delete: boolean;
    is_take_out: boolean;
    remark: any;
    subtotal_before_discount: number;
    invoice_combo_itemized?: any[];
    customer_id?: number | undefined;
    guid?: any | undefined;
    line_subtotal_for_display?: number | undefined;
}
