export interface INoteGroup {
    id?: number;
    name?: string;
    is_delete?: boolean;  
}
