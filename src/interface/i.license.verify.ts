export interface ILicenseVerify {
    action?: number;
    quotas: any[];
    totp?: string;
}
