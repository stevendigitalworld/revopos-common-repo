export interface IAttribute {
    attribute_item: Array<IAttributeItem>;
    error: any;
    id: Number;
    name: String;
    alias: String;
}

export interface IAttributeItem {
    id: Number;
    attribute_id: Number;
    name: String;
    alias: String;
}

export interface IVariantDefinition {
    attribute_id: Number;
    attribute_item_id: Number;
}

export interface IVariantItem {
    name: string;
    alias: string;
    price: number;
    cost: number;
    stock: number;
    codename: string;
    barcode: string;
    expand: boolean;
}