import { IChartDataSet } from './i.chart-data-set';
export interface IChartData {
    model?: number;
    time?: number;
    dataset?: Array<IChartDataSet>;
    start_datetime?: any;
    end_datetime?: any;
}
