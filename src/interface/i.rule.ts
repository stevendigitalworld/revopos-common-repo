import { RuleItemModel } from "../class/rule-item-model";

export interface IRule {
    above_invoice_amount?: number | undefined;
    applying_type?: number | undefined;
    datetime_create?: Date | undefined;
    discount_amount?: number | undefined;
    discount_calc_type?: number | undefined;
    discount_percent?: number | undefined;
    id?: number | undefined;
    is_delete?: boolean | undefined;
    model?: number | undefined;
    rule_item?: Array<RuleItemModel>;
    type?: number | undefined;
}