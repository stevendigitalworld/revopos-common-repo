import { IItem } from './i.item';

export interface IRuleItem {
    category_id?: number;
    datetime_create?: string;
    id?: number;
    is_delete?: boolean;
    item?: IItem[];
    item_id?: number;
    quota_order?: number;
    rule_id?: number;
}
