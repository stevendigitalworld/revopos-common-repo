
export interface IPaymentMethod {
    subtotal: number;
    id?: number;
    name: string;
    name_abbrev: string;
    is_cash: boolean;
    type: number;
    action?: number;
    payment_currency_id?: number;
    enable_drawer?: boolean;
    enable_receipt?: boolean;
    is_disabled?: boolean;
    is_delete?: boolean;
    image_path?: string;
    sequence?: number;
    payment_method_code?: string;
    codename?: string;
    is_integrated?: boolean;
    is_generated?: boolean;
    api_id?: string;
    api_key?: string;
    datetime_create?: any;
    timestamp_update?: number;
    currency_code?: string;
    is_sales_exclusive: boolean;
}
