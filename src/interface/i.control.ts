import { ControlActionCode } from './../code/control-action-code';
export interface IControl {
    version?: string;
    platform?: string;
    platform_version?: number;
    uid?: string;
    type?: number;
    action? : ControlActionCode;
}
