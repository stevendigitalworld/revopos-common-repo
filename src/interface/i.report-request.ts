import { EmailModel } from './../class/email-model';
export interface IReportRequest {
    type?: number;
    datetime_start?: string;
    datetime_end?: string;
    operation: any;
    template: any;
    action?: number;
    email?: EmailModel;
    automate?: boolean;
    schedule_type?: number;
}