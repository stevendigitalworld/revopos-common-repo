export interface IChartDataSet {
    data?: number;
    label?: string;
}
