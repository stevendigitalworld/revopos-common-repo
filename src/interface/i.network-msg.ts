export interface INetworkMsg {
    ip?:string;
    data?: {
        is_online?: boolean;
        is_retrying?: boolean;
    }
}