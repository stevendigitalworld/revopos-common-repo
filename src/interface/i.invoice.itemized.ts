
export interface IInvoiceItemized {
    invoice_itemized_modifier?: any[];
    invoice_itemized_user?: any[];
    id?: number | undefined;
    invoice_line_id?: number | undefined;
    actual_price?: number | undefined;
    unit_price: number;
    actual_price_before_discount?: number | undefined;
    discount_calc_type: number;
    discount_amount: number;
    discount_percent: number;
    remark: any;
    is_delete: number;
    state: number;
    guid: any;
}
