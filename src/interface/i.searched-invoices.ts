import { IInvoice } from './i.invoice';
export interface ISearchedInvoices {
    count_all?: number;
    invoice?: Array<IInvoice>
}