export interface ICategory {
    id?: number | undefined;
    name?: string| undefined;
    alias?: string;
    description?: string| undefined;
    sequence?: 0;
    is_soup_base?: number;
    is_delete?: number;
}
