import { RecipientModel } from './../class/recipient-model';

export interface IEmail {
    recipients?: RecipientModel[];
    attachment?: string[];
    subject?: string;
    content?: string;
}
