import { ICustomer } from './i.customer';
export interface ISearchedCustomers {
    count_all?: number;
    data?: Array<ICustomer>;
    errors: any;
}