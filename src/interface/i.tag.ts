export interface ITag {
    id?: number | undefined;
    name?: string | undefined;
    model?: string | undefined;
}