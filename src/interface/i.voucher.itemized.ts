export interface IVoucherItemized {
    voucher_id?: number;
    quantity?: number;
    discount_amount?: number;
}
