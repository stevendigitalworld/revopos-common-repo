import { IRecipient } from './i.recipient';
export interface IQueAccount {
    id?: number;
    tenancy_id?: number;
    tenancy_guid?:String;
    type?: string;
    name?: string;
    image_base64?: string;
    image_url?: string;
    action?: string;
    email?: string;
    password?: string;
    new_password?: string;
    phone_no?: string;
    phone_country_code?: string;
    is_email_confirmed?: boolean;
    email_otp?: string;
    input_otp?: string;
    recipients: Array<IRecipient>;
    datetime_create?: string;
    datetime_update?: string;
    timestamp_update?: number;
    invitation_code?: string;
    company_name?: string;
    is_owner?: boolean;
    is_delete?: boolean;
    error?: any;
}