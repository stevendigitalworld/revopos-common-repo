export interface IDelivery {
    id?: number | undefined;
    action?: number | undefined;
    datetime_order?: any | undefined;
    delivery_by?: any | undefined;
    invoice?: any | undefined;
    invoice_id?: number | undefined;
    is_delete?: boolean | undefined;
    json?: string | undefined;
    server_delivery_id?: number | undefined;
    status?: number | undefined;
}