import { ComboGroupModel } from './../class/combo-group-model';
export interface ICombo {
    combo_group : Array<ComboGroupModel>;
    id?: number;
    is_delete?: false;
    is_group?: true;
    is_one_click?: false;
    is_upsizeable?: false;
    item_id?: number;
    upsize_price?: number;
}
