import { IPackageItem } from "./i.package.item";

export interface IPackage {
    package_item: IPackageItem[];
    item_id?: number;
    price?: number;
    is_delete?: boolean;
}
