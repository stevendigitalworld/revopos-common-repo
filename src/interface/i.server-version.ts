export interface IServerVersion {
    channel?: string | undefined;
    datetime?: string | undefined;
    device?: any;
    is_delete?: boolean | undefined;
    revopos_database_current?: string | undefined;
    revopos_database_target?: string | undefined;
    revopos_server?: string | undefined;
    revopos_electron_current?: any | undefined;
    revopos_electron_target?: any | undefined;
}