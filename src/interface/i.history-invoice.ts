import { IInvoice } from './i.invoice';

export interface IHistoryInvoice {
    id?: number;
    action?: number;
    datetime?: Date;
    device_id?: number;
    invoice?: IInvoice;
    invoice_id?: number;
    json?: string;
}