export interface IAccount {
    password_new?: string;
    shop?: any;
    selected_shop?: string;
    deposit_amount_sms?: number;
    authorized_email?: string;
    license?: number;
    license_request?: null;
    action?: number;
    stripe_token?: null;
    stripe_customer?: null;
    stripe_subscription?: null;
    error?: number;
    otp?: number;
    id?: number;
    stripe_customer_id?: null;
    name?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    password?: string;
    phone_country_code?: string;
    phone_no?: number;
    role?: number;
    is_email_confirmed?: boolean;
}