export interface INote {
    datetime?: Date | undefined;
    group_id?: number | undefined;
    id?: number | undefined;
    is_delete?: boolean | undefined;
    note?: string | undefined;
    type?: number | undefined;
    usage?: number | undefined;
}