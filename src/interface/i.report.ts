
export interface IReport {
    id?: number;
    type?: number;
    filename?: string;
    content?: any;
    datetime_create?: string;
    datetime_update?: string;        
}
