import { ICustomer } from './i.customer';
import { IUser } from './i.user';
export interface IAppointment {
    color?: string;
    customer?: ICustomer;
    customer_id?: number;
    datetime_create?: string;
    datetime_end?: string;
    datetime_start?: string;
    id?: number;
    is_delete?: boolean;
    remark?: any;
    title?: string;
    type?: number;
    user: IUser; 
    user_id?: number;
}
